<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use SoftDeletes;

    # See App/User @reference 1
    public function users() {
        return $this->belongsToMany('App/User');
    }
}
