<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * This model is the core of the applications since the Instrumentality app is user driven.
 * We will use this model extensively in the machine learning module.
 */
class User extends Model
{
    use SoftDeletes;

    # @reference 1
    # This method defines the relationship that users have with the tasks
    # More precisesly, it defines that a user can be assigned mutiple tasks and a task can e assigned to multiple users
    # This method has a sibling method written in App/Task that gets all the users that are assigned to a specific task
    public function tasks() {
        return $this->belongsToMany('App/Task');
    }
}
