<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Task;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {
    $tech_verbs = ["implement", "utilize", "integrate", "streamline", "optimize", "evolve", "transform", "embrace", 
    "enable", "orchestrate", "leverage", "reinvent", "aggregate", "architect", "enhance", "incentivize", "morph", "empower", 
    "envisioneer", "monetize", "harness", "facilitate", "seize", "disintermediate", "synergize", "strategize", "deploy", 
    "brand", "grow", "target", "syndicate", "synthesize", "deliver", "mesh", "incubate", "engage", "maximize", "benchmark", 
    "expedite", "reintermediate", "whiteboard", "visualize", "repurpose", "innovate", "scale", "unleash", "drive", "extend", 
    "engineer", "revolutionize", "generate", "exploit", "transition", "e-enable", "iterate", "cultivate", "matrix", 
    "productize", "redefine", 
    "recontextualize"];

    $tech_adjectives = ["clicks-and-mortar", "value-added", "vertical", "proactive", "robust", "revolutionary", "scalable", 
    "leading-edge", "innovative", "intuitive", "strategic", "e-business", "mission-critical", "sticky", "one-to-one", 
    "24/7", "end-to-end", "global", "B2B", "wearable", "granular", "frictionless", "virtual", "viral", "dynamic", "24/365", 
    "best-of-breed", "killer", "magnetic", "bleeding-edge", "web-enabled", "interactive", "dot-com", "sexy", "back-end", 
    "real-time", "efficient", "front-end", "distributed", "seamless", "extensible", "turn-key", "world-class", 
    "open-source", "cross-platform", "cross-media", "synergistic", "bricks-and-clicks", "out-of-the-box", "enterprise", 
    "integrated", "impactful", "wireless", "transparent", "next-generation", "cutting-edge", "user-centric", "visionary", 
    "customized", "ubiquitous", "plug-and-play", "collaborative", "compelling", "holistic", "rich"];

    $tech_nouns = ["synergies", "web-readiness", "paradigms", "markets", "partnerships", "infrastructures", "platforms", 
    "initiatives", "channels", "eyeballs", "communities", "hashtags", "solutions", "e-tailers", "e-services", "action-items", 
    "portals", "niches", "technologies", "content", "vortals", "supply-chains", "convergence", "relationships", 
    "architectures", "interfaces", "e-markets", "e-commerce", "systems", "bandwidth", "infomediaries", "models", 
    "mindshare", "deliverables", "users", "schemas", "networks", "applications", "metrics", "e-business", "functionalities", 
    "experiences", "web services", "methodologies"];

    return [
        'task_name' => ucfirst($faker->randomElement($tech_verbs)) . ' ' . $faker->randomElement($tech_adjectives) . ' ' . $faker->randomElement($tech_nouns),
        'task_description' => $faker->paragraph(5, true),
        'due_date' => $faker->dateTimeBetween('now', '+1 years'),
        'due_time' => $faker->time('H:i:s', '21:00:00')
    ];
});
