<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TasksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Creating 500 tasks: ~ 5 tasks/user
        $tasks = factory(App\Task::class, 500)->create();

        // Getting all the users that were created previously
        $users = App\User::all();

        $assigned_tasks = [];

        // For each task let's pick some random users and assign that task to the picked users
        foreach ($tasks as $task) {
            $numberOfUsers = rand (1,5); // <- 1. Pick a random number of users

            $picked_users = $users->random($numberOfUsers);

            foreach ($picked_users as $key => $user) {
                array_push(
                    $assigned_tasks,
                    ['user_id' => $user->id, 'task_id' => $task->id, 'assigned_at' => date_create(), 'completed' => false]
                );
            }

        }

        DB::table('tasks_users')->insert($assigned_tasks); // <- 4. Inserting the assigned tasks
    }
}
