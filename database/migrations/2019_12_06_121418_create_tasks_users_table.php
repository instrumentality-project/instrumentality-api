<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();

        Schema::create('tasks_users', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('task_id');
            $table->timestamp('assigned_at');
            $table->boolean('completed');

            // Defining the composite primary key of the pivot table
            $table->primary(['user_id', 'task_id']);
            
            // Defining foreign keys for referencing the other two tables
            $table ->foreign('user_id')
                   ->references('id')
                   ->on('users');

            $table ->foreign('task_id')
                   ->references('id')
                   ->on('tasks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks_users');
    }
}
